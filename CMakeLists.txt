cmake_minimum_required(VERSION 2.8)
project(xsdl)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules")

find_package(SDL2 REQUIRED)

file(GLOB_RECURSE XSDL_HEADERS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "include/*.hpp")
file(GLOB_RECURSE XSDL_PRIVATE_HEADERS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "source/include/*.hpp")
file(GLOB_RECURSE XSDL_SOURCES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "source/*.cpp")

add_library(xsdl STATIC ${XSDL_HEADERS} ${XSDL_PRIVATE_HEADERS} ${XSDL_SOURCES})
target_link_libraries(xsdl PUBLIC sdl2 geom)
target_include_directories(xsdl
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}/include
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/source/include
)
