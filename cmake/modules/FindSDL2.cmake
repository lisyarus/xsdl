set(SDL2_ROOT "" CACHE PATH "Prefix of SDL2 installation")

if(NOT SDL2_FOUND)

	include(FindPackageHandleStandardArgs)

	find_path(SDL2_INCLUDE_DIR SDL2/SDL.h
		HINTS ${SDL2_ROOT}/include
	)

	find_library(SDL2_LIBRARY NAMES SDL2
		HINTS ${SDL2_ROOT}/lib
	)

	find_package_handle_standard_args(SDL2 DEFAULT_MSG SDL2_INCLUDE_DIR SDL2_LIBRARY)

	if(SDL2_FOUND)

		add_library(sdl2 SHARED IMPORTED)
		set_target_properties(sdl2 PROPERTIES
			IMPORTED_LOCATION ${SDL2_LIBRARY}
			INTERFACE_INCLUDE_DIRECTORIES ${SDL2_INCLUDE_DIR}
		)

		mark_as_advanced(SDL2_INCLUDE_DIR SDL2_LIBRARY)

	endif()

endif()
