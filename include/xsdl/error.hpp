#pragma once

#include <stdexcept>

namespace xsdl
{

	struct error_t
		: std::runtime_error
	{
		error_t (std::string const & message = {});
	};

}
