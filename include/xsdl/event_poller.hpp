#pragma once

#include <xsdl/window.hpp>

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>

#include <functional>
#include <memory>

namespace xsdl
{

	struct event_poller_t
	{
		using disconnect_callback_t = std::function<void()>;

		struct timer_t
		{
			struct impl;

		private:
			std::unique_ptr<impl> pimpl_;

		public:

			timer_t (std::unique_ptr<impl> pimpl);

			timer_t (timer_t &&);

			using callback_t = std::function<void()>;

			event_poller_t::disconnect_callback_t connect (callback_t);

			void stop ( );

			~ timer_t ( );
		};

		event_poller_t ( );
		~ event_poller_t ( );

		using on_mouse_move_t = std::function<void(window_t::id_t, geom::point_2i, geom::vector_2i)>;
		using on_wheel_t = std::function<void(window_t::id_t, double)>;

		using on_button_t = std::function<void(window_t::id_t)>;

		using on_key_t = std::function<void(window_t::id_t, std::int32_t)>;

		using on_resize_t = std::function<void(window_t::id_t, window_t::size_t)>;
		using on_close_t = std::function<void(window_t::id_t)>;

		using on_quit_t = std::function<void()>;


		// Event callbacks

		disconnect_callback_t on_mouse_move (on_mouse_move_t);
		disconnect_callback_t on_wheel (on_wheel_t);

		disconnect_callback_t on_left_button_down (on_button_t);
		disconnect_callback_t on_left_button_up (on_button_t);
		disconnect_callback_t on_middle_button_down (on_button_t);
		disconnect_callback_t on_middle_button_up (on_button_t);
		disconnect_callback_t on_right_button_down (on_button_t);
		disconnect_callback_t on_right_button_up (on_button_t);

		disconnect_callback_t on_key_down (on_key_t);
		disconnect_callback_t on_key_up (on_key_t);

		disconnect_callback_t on_resize (on_resize_t);
		disconnect_callback_t on_close (on_close_t);

		disconnect_callback_t on_quit (on_quit_t);


		// State queries

		bool left_button_down ( ) const;
		bool middle_button_down( ) const;
		bool right_button_down( ) const;

		bool key_down (std::int32_t key) const;


		timer_t create_timer (std::uint32_t period);

		std::uint64_t poll ( );
		std::uint64_t wait ( );

	private:
		struct impl;
		std::unique_ptr<impl> pimpl_;
	};


}
