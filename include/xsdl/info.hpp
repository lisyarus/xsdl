#pragma once

#include <SDL2/SDL_version.h>

#include <string>

namespace xsdl
{

	SDL_version version ( );

	std::string platform ( );

	std::string video_driver ( );

}

template <typename OStream>
OStream & operator << (OStream & os, SDL_version v)
{
	return os << (int)v.major << '.' << (int)v.minor << '.' << (int)v.patch;
}
