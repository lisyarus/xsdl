#pragma once

#include <SDL2/SDL_keycode.h>

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>

#include <string>
#include <cstdint>
#include <memory>
#include <functional>

namespace xsdl
{

	struct initializer_t
	{
		initializer_t ( );

		initializer_t (std::uint32_t flags);

		~ initializer_t ( );
	};

}
