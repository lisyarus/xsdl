#pragma once

#include <xsdl/window.hpp>

#include <optional>

namespace xsdl
{

	struct gl_context_t
	{
		struct version_t
		{
			std::uint32_t major, minor;
			bool compatibility;
		};

		gl_context_t (window_t & window, version_t version = {3, 3, false}, std::optional<std::uint32_t> multisample = std::nullopt);

		void swap_buffers ( );

		void make_current ( );

		void vsync (bool enable);

		~ gl_context_t ( );

	private:
		void * handle_;
		window_t::handle_t * window_handle_;
	};

	struct gl_window_t
		: window_t
		, gl_context_t
	{
		gl_window_t (event_poller_t & ep, std::string const & title, std::uint32_t flags, size_t size = {800, 600}, version_t version = {3, 3, false}, std::optional<std::uint32_t> multisample = std::nullopt);
		gl_window_t (event_poller_t & ep, std::string const & title, size_t size = {800, 600}, version_t version = {3, 3, false}, std::optional<std::uint32_t> multisample = std::nullopt);
	};

}
