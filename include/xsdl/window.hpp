#pragma once

#include <cstdint>
#include <string>
#include <functional>

namespace xsdl
{

	struct event_poller_t;

	struct window_t
	{
		struct handle_t;

		struct size_t
		{
			std::int32_t width;
			std::int32_t height;
		};

		typedef std::uint32_t id_t;

		window_t ( );
		window_t (window_t &&);
		window_t (event_poller_t & ep, std::string const & title, std::uint32_t flags, size_t size = {800, 600});
		window_t (event_poller_t & ep, std::string const & title, size_t size = {800, 600});

		~ window_t ( );

		void show ( );

		handle_t * handle ( );

		id_t id ( );

		size_t size ( ) const;

		explicit operator bool ( ) const;
		bool operator ! ( ) const;

	private:
		handle_t * handle_;
		size_t size_;
		std::function<void()> disconnect_;
	};

}
