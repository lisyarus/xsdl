#include <xsdl/error.hpp>

#include <SDL2/SDL_error.h>

#include <string>

namespace xsdl
{

	static std::string error_string ( )
	{
		return SDL_GetError();
	}

	error_t::error_t (std::string const & message)
		: std::runtime_error(message.empty() ? error_string() : message + ": " + error_string())
	{ }

}
