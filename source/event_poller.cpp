#include <xsdl/event_poller.hpp>

#include <xsdl/private/event_poller_impl.hpp>

#include <SDL2/SDL_events.h>

namespace xsdl
{

	event_poller_t::event_poller_t ( )
		: pimpl_(new impl())
	{ }

	event_poller_t::~ event_poller_t ( )
	{ }

	using detail::connection_t;

	event_poller_t::disconnect_callback_t event_poller_t::on_mouse_move (on_mouse_move_t f)
	{
		return connection_t(pimpl_->move_sig.connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_wheel (on_wheel_t f)
	{
		return connection_t(pimpl_->wheel_sig.connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_left_button_down (on_button_t f)
	{
		return connection_t(pimpl_->button_down_sig[SDL_BUTTON_LEFT].connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_left_button_up (on_button_t f)
	{
		return connection_t(pimpl_->button_up_sig[SDL_BUTTON_LEFT].connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_middle_button_down (on_button_t f)
	{
		return connection_t(pimpl_->button_down_sig[SDL_BUTTON_MIDDLE].connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_middle_button_up (on_button_t f)
	{
		return connection_t(pimpl_->button_up_sig[SDL_BUTTON_MIDDLE].connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_right_button_down (on_button_t f)
	{
		return connection_t(pimpl_->button_down_sig[SDL_BUTTON_RIGHT].connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_right_button_up (on_button_t f)
	{
		return connection_t(pimpl_->button_up_sig[SDL_BUTTON_RIGHT].connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_key_down (on_key_t f)
	{
		return connection_t(pimpl_->keydown_sig.connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_key_up (on_key_t f)
	{
		return connection_t(pimpl_->keyup_sig.connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_resize (on_resize_t f)
	{
		return connection_t(pimpl_->resize_sig.connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_close (on_close_t f)
	{
		return connection_t(pimpl_->close_sig.connect(std::move(f)));
	}

	event_poller_t::disconnect_callback_t event_poller_t::on_quit (on_quit_t f)
	{
		return connection_t(pimpl_->quit_sig.connect(std::move(f)));
	}

	bool event_poller_t::left_button_down ( ) const
	{
		return pimpl_->left_button_down;
	}

	bool event_poller_t::middle_button_down( ) const
	{
		return pimpl_->middle_button_down;
	}

	bool event_poller_t::right_button_down( ) const
	{
		return pimpl_->right_button_down;
	}

	bool event_poller_t::key_down (std::int32_t key) const
	{
		return pimpl_->key_down[key];
	}

	event_poller_t::timer_t event_poller_t::create_timer (std::uint32_t period)
	{
		return pimpl_->create_timer(period);
	}

	std::uint64_t event_poller_t::poll ( )
	{
		return pimpl_->poll(true);
	}

	std::uint64_t event_poller_t::wait ( )
	{
		return pimpl_->poll(false);
	}

}
