#pragma once

#include <xsdl/event_poller.hpp>

#include <boost/signals2/signal.hpp>

#include <unordered_map>

namespace xsdl
{

	struct event_poller_t::impl
	{
		std::uint32_t timer_event;

		template <typename Signature>
		using signal_type = boost::signals2::signal<Signature>;

		signal_type<void()> quit_sig;

		signal_type<void(window_t::id_t, geom::point_2i, geom::vector_2i)> move_sig;

		signal_type<void(window_t::id_t, double)> wheel_sig;

		std::array<signal_type<void(window_t::id_t)>, 4> button_down_sig;
		std::array<signal_type<void(window_t::id_t)>, 4> button_up_sig;

		signal_type<void(window_t::id_t, std::int32_t)> keydown_sig;
		signal_type<void(window_t::id_t, std::int32_t)> keyup_sig;

		signal_type<void(window_t::id_t, window_t::size_t)> resize_sig;
		signal_type<void(window_t::id_t)> close_sig;

		bool left_button_down;
		bool middle_button_down;
		bool right_button_down;

		std::unordered_map<std::int32_t, bool> key_down;

		std::unordered_map<int, signal_type<void()>> timer_sig_map;

		impl ( );

		event_poller_t::timer_t create_timer (std::uint32_t period);

		std::uint64_t poll (bool nowait);
	};

	namespace detail
	{

		struct connection_t
		{
			connection_t (boost::signals2::connection c)
				: c_(c)
			{ }

			void operator() ( )
			{
				c_.disconnect();
			}

			boost::signals2::connection c_;
		};

	}

}
