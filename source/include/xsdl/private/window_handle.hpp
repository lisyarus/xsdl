#pragma once

#include <xsdl/window.hpp>

#include <SDL2/SDL_video.h>

namespace xsdl
{

	struct window_t::handle_t
	{
		SDL_Window * cast ( )
		{
			return reinterpret_cast<SDL_Window *>(this);
		}
	};

}
