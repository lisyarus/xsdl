#include <xsdl/info.hpp>

#include <SDL2/SDL_platform.h>
#include <SDL2/SDL_video.h>

namespace xsdl
{

	SDL_version version ( )
	{
		SDL_version v;
		SDL_GetVersion(&v);
		return v;
	}

	std::string platform ( )
	{
		return SDL_GetPlatform();
	}

	std::string video_driver ( )
	{
		return SDL_GetCurrentVideoDriver();
	}

}
