#include <xsdl/init.hpp>
#include <xsdl/error.hpp>

#include <SDL2/SDL.h>

namespace xsdl
{

	initializer_t::initializer_t ( )
		: initializer_t(SDL_INIT_VIDEO | SDL_INIT_TIMER)
	{ }

	initializer_t::initializer_t (std::uint32_t flags)
	{
		if (SDL_Init(flags))
			throw error_t("SDL_Init");
	}

	initializer_t::~ initializer_t ( )
	{
		SDL_Quit();
	}



}
