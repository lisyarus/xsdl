#include <xsdl/opengl.hpp>

#include <xsdl/private/window_handle.hpp>

#include <SDL2/SDL_video.h>

namespace xsdl
{

	static void * create_gl_context (window_t & window, gl_context_t::version_t version, std::optional<std::uint32_t> multisample)
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, version.major);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, version.minor);
		if (version.major < 3 || (version.major == 3 && version.minor < 3))
			 SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, 0);
		else
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
				version.compatibility ? SDL_GL_CONTEXT_PROFILE_COMPATIBILITY : SDL_GL_CONTEXT_PROFILE_CORE);
		if (multisample)
		{
			SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
			SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, *multisample);
		}
		return SDL_GL_CreateContext(window.handle()->cast());
	}

	gl_context_t::gl_context_t (window_t & window, version_t version, std::optional<std::uint32_t> multisample)
		: handle_(create_gl_context(window, version, multisample))
		, window_handle_(window.handle())
	{ }

	void gl_context_t::swap_buffers ( )
	{
		SDL_GL_SwapWindow(window_handle_->cast());
	}

	void gl_context_t::make_current ( )
	{
		SDL_GL_MakeCurrent(window_handle_->cast(), handle_);
	}

	void gl_context_t::vsync (bool enable)
	{
		SDL_GL_SetSwapInterval(enable ? 1 : 0);
	}

	gl_context_t::~ gl_context_t ( )
	{
		SDL_GL_DeleteContext(handle_);
	}

	gl_window_t::gl_window_t (event_poller_t & ep, std::string const & title, std::uint32_t flags, size_t size, version_t version, std::optional<std::uint32_t> multisample)
		: window_t(ep, title, flags | SDL_WINDOW_OPENGL, size)
		, gl_context_t(static_cast<window_t &>(*this), version, multisample)
	{ }

	gl_window_t::gl_window_t (event_poller_t & ep, std::string const & title, size_t size, version_t version, std::optional<std::uint32_t> multisample)
		: gl_window_t(ep, title, SDL_WINDOW_RESIZABLE, size, version, multisample)
	{ }


}
