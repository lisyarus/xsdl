#include <xsdl/private/event_poller_impl.hpp>

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_timer.h>

namespace xsdl
{

	struct event_poller_t::timer_t::impl
	{
		int id;
		event_poller_t::impl * ep_impl;

		~ impl ( );
	};

	event_poller_t::timer_t::impl::~ impl ( )
	{
		SDL_RemoveTimer(id);
		ep_impl->timer_sig_map.erase(id);
	}

	event_poller_t::impl::impl ( )
	{
		left_button_down = false;
		middle_button_down = false;
		right_button_down = false;

		timer_event = SDL_RegisterEvents(1);
	}

	event_poller_t::timer_t::timer_t (std::unique_ptr<impl> pimpl)
		: pimpl_(std::move(pimpl))
	{ }

	event_poller_t::timer_t::timer_t (event_poller_t::timer_t &&) = default;

	event_poller_t::disconnect_callback_t event_poller_t::timer_t::connect (callback_t f)
	{
		return detail::connection_t(pimpl_->ep_impl->timer_sig_map[pimpl_->id].connect(f));
	}

	void event_poller_t::timer_t::stop ( )
	{
		pimpl_.reset();
	}

	event_poller_t::timer_t::~ timer_t ( )
	{ }

	static std::uint32_t timer_callback (std::uint32_t interval, event_poller_t::timer_t::impl * self)
	{
		SDL_Event event;
		memset(&event, 0, sizeof(event));
		event.type = self->ep_impl->timer_event;
		event.user.code = self->id;
		SDL_PushEvent(&event);
		return interval;
	}

	event_poller_t::timer_t event_poller_t::impl::create_timer (std::uint32_t period)
	{
		auto timer_pimpl = std::make_unique<event_poller_t::timer_t::impl>();
		timer_pimpl->id = SDL_AddTimer(period, reinterpret_cast<SDL_TimerCallback>(timer_callback), timer_pimpl.get());
		timer_pimpl->ep_impl = this;
		return event_poller_t::timer_t(std::move(timer_pimpl));
	}

	std::uint64_t event_poller_t::impl::poll (bool nowait)
	{
		std::uint64_t count = 0;

		SDL_Event event;

		auto poller = [&event, &nowait]{
			if (nowait)
			{
				return SDL_PollEvent(&event);
			}
			else
			{
				nowait = true;
				return SDL_WaitEvent(&event);
			}
		};

		while (poller())
		{
			++count;
			switch (event.type)
			{
			case SDL_QUIT:
				quit_sig();
				break;
			case SDL_MOUSEMOTION:
				move_sig(
					event.window.windowID,
					{event.motion.x, event.motion.y},
					{event.motion.xrel, event.motion.yrel}
				);
				break;
			case SDL_MOUSEWHEEL:
				wheel_sig(event.window.windowID, event.wheel.y);
				break;
			case SDL_MOUSEBUTTONDOWN:
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					left_button_down = true; break;
				case SDL_BUTTON_MIDDLE:
					middle_button_down = true; break;
				case SDL_BUTTON_RIGHT:
					right_button_down = true; break;
				}
				button_down_sig[event.button.button](event.window.windowID);
				break;
			case SDL_MOUSEBUTTONUP:
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					left_button_down = false; break;
				case SDL_BUTTON_MIDDLE:
					middle_button_down = false; break;
				case SDL_BUTTON_RIGHT:
					right_button_down = false; break;
				}
				button_up_sig[event.button.button](event.window.windowID);
				break;
			case SDL_KEYDOWN:
				key_down[event.key.keysym.sym] = true;
				keydown_sig(event.window.windowID, event.key.keysym.sym);
				break;
			case SDL_KEYUP:
				key_down[event.key.keysym.sym] = false;
				keyup_sig(event.window.windowID, event.key.keysym.sym);
				break;
			case SDL_WINDOWEVENT:
				switch (event.window.event)
				{
				case SDL_WINDOWEVENT_RESIZED:
					resize_sig(event.window.windowID, {event.window.data1, event.window.data2});
					break;
				case SDL_WINDOWEVENT_CLOSE:
					close_sig(event.window.windowID);
					break;
				}
				break;
			}

			if (event.type == timer_event)
			{
				if (timer_sig_map.count(event.user.code))
					timer_sig_map[event.user.code]();
			}
		}

		return count;
	}

}
