#include <xsdl/window.hpp>
#include <xsdl/event_poller.hpp>

#include <xsdl/private/window_handle.hpp>

#include <stdexcept>

namespace xsdl
{

	window_t::window_t ( )
		: handle_(nullptr)
	{ }

	window_t::window_t (window_t && w)
		: handle_(w.handle_)
		, size_(w.size_)
	{
		w.handle_ = nullptr;
	}

	window_t::window_t (event_poller_t & ep, std::string const & title, std::uint32_t flags, size_t size)
		: handle_(reinterpret_cast<handle_t *>(SDL_CreateWindow(title.data(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, size.width, size.height, flags)))
		, size_(size)
	{
		disconnect_ = ep.on_resize([this](id_t id, size_t size){
			if (this->id() == id)
				this->size_ = size;
		});
	}

	window_t::window_t (event_poller_t & ep, std::string const & title, size_t size)
		: window_t(ep, title, SDL_WINDOW_RESIZABLE, size)
	{ }

	window_t::~ window_t ( )
	{
		disconnect_();

		if (handle())
			SDL_DestroyWindow(handle_->cast());
	}

	void window_t::show ( )
	{
		if (!handle())
			throw std::runtime_error("null window");
		SDL_ShowWindow(handle_->cast());
	}

	window_t::handle_t * window_t::handle ( )
	{
		return handle_;
	}

	id_t window_t::id ( )
	{
		if (!handle())
			throw std::runtime_error("null window");
		return SDL_GetWindowID(handle_->cast());
	}

	window_t::size_t window_t::size ( ) const
	{
		return size_;
	}

	window_t::operator bool ( ) const
	{
		return handle_ != nullptr;
	}

	bool window_t::operator ! ( ) const
	{
		return handle_ == nullptr;
	}

}
